import { useEffect, useState } from "react";

const BasicForm = () => {
    const [firstName, setFirstName] = useState(localStorage.getItem("firstname") || "");
    const [lastName, setLastName] = useState(localStorage.getItem("lastname") || "");

    const inputFirstNameChangeHandler = (event) => {
        let value = event.target.value;
        localStorage.setItem("firstname", value);
        setFirstName(value);
    }

    const inputLastNameChangeHandler = (event) => {
        let value = event.target.value;
        localStorage.setItem("lastname", value);
        setLastName(value);
    }

    useEffect(() => {
        document.title = `${firstName} ${lastName}`
    }, [firstName, lastName])

    return (
        <div>
            <input value={firstName} onChange={inputFirstNameChangeHandler}/>
            <br />
            <input value={lastName} onChange={inputLastNameChangeHandler}/>
            <br />
            <p>{firstName} {lastName}</p>
        </div>
    )
}

export default BasicForm;